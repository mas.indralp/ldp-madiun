<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="index.html">
              <i class="icon-grid menu-icon"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <!-- MENU MARKETING / NIAGA -->
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-admin-marketing" aria-expanded="false" aria-controls="ui-cust">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">Marketing / Niaga</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-admin-marketing">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="customer">Customer</a></li>               
                <li class="nav-item"> <a class="nav-link" href="daily">Daily Progress</a></li>               
                <li class="nav-item"> <a class="nav-link" href="req">Req Logistik</a></li>               
                <li class="nav-item"> <a class="nav-link" href="bod">BOD</a></li>               
                <li class="nav-item"> <a class="nav-link" href="crm">CRM</a></li>               
                <li class="nav-item"> <a class="nav-link" href="laporan">Laporan</a></li>               
              </ul>
            </div>
          </li>
          <!-- END MENU MARKETING / NIAGA -->

          <!-- MENU SUB MARKETING / NIAGA -->
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-sub-marketing" aria-expanded="false" aria-controls="ui-cust">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">Staff Marketing</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-sub-marketing">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="customer">Customer</a></li>               
                <li class="nav-item"> <a class="nav-link" href="daily">Daily Progress</a></li>               
                <li class="nav-item"> <a class="nav-link" href="req">Req Logistik</a></li>     
                <li class="nav-item"> <a class="nav-link" href="crm">CRM</a></li>                   
              </ul>
            </div>
          </li>
          <!-- END MENU SUB MARKETING / NIAGA -->

          <!-- MENU TRANSTER -->
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-transter" aria-expanded="false" aria-controls="ui-cust">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">Transter</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-transter">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="customer">Project</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Daily Progress</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Customer</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Req Logistik</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">List Project</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Survey</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Installasi</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">CRM</a></li>                 
              </ul>
            </div>
          </li>
          <!-- END MENU TRANSTER -->
          <!-- MENU STAFF/SUB TRANSTER -->
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-sub-transter" aria-expanded="false" aria-controls="ui-cust">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">Staff Transter</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-sub-transter">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="customer">Project</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Daily Progress</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Req Logistik</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">CRM</a></li>               
              </ul>
            </div>
          </li>
          <!-- END MENU STAFF/SUB TRANSTER -->
          <!-- MENU LOGISTIK -->
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-logistik" aria-expanded="false" aria-controls="ui-cust">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">Logistik</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-logistik">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="customer">Log. Masuk</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Log. Keluar</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Barang</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Vendor</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Req Logistik</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">List Request</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Laporan</a></li>               
              </ul>
            </div>
          </li>
          <!-- END MENU LOGISTIK -->
          <!-- MENU NOC -->
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-NOC" aria-expanded="false" aria-controls="ui-cust">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">NOC</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-NOC">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="customer">Troubleshoot</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Downgrade/Upgrade</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Monitoring</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Req Logistik</a></li>               
              </ul>
            </div>
          </li>
          <!-- END MENU NOC -->
          <!-- MENU CUSTOMER -->
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-CUSTOMER" aria-expanded="false" aria-controls="ui-cust">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">Customer</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-CUSTOMER">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="customer">Data Customer</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Req Down/Up Grade</a></li>               
                <li class="nav-item"> <a class="nav-link" href="customer">Invoice</a></li> >               
              </ul>
            </div>
          </li>
          <!-- END MENU CUSTOMER -->
          <!-- MENU ADMINISTRATOR -->
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-ADMINISTRATOR" aria-expanded="false" aria-controls="ui-cust">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">Administrator</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-ADMINISTRATOR">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="customer">Data Baru</a></li>               
              </ul>
            </div>
          </li>
          <!-- END MENU ADMINISTRATOR -->


          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-cust" aria-expanded="false" aria-controls="ui-cust">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">Data Customer</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-cust">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="customer">Data Baru</a></li>
                <li class="nav-item"> <a class="nav-link" href="listcustomer">List Customer</a></li>
                <li class="nav-item"> <a class="nav-link" href="customer_1">Customer Marketing</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-barang" aria-expanded="false" aria-controls="ui-barang">
              <i class="ti-shopping-cart-full menu-icon"></i>
              <span class="menu-title">Data Barang</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-barang">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="barang">Data Baru</a></li>
                <li class="nav-item"> <a class="nav-link" href="listbarang">List Barang</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-karyawan" aria-expanded="false" aria-controls="ui-karyawan">
              <i class="ti-user menu-icon"></i>
              <span class="menu-title">Data Karyawan</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-karyawan">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="karyawan">Data Baru</a></li>
                <li class="nav-item"> <a class="nav-link" href="listkaryawan">List Karyawan</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-logistik" aria-expanded="false" aria-controls="ui-logistik">
              <i class="ti-target menu-icon"></i>
              <span class="menu-title">Data Logistik</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-logistik">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="logistik">Data Baru</a></li>
                <li class="nav-item"> <a class="nav-link" href="listlogistik">List Logistik</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-vendor" aria-expanded="false" aria-controls="ui-vendor">
              <i class="ti-layers-alt menu-icon"></i>
              <span class="menu-title">Data Vendor</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-vendor">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="vendor">Data Baru</a></li>
                <li class="nav-item"> <a class="nav-link" href="listvendor">List Vendor</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-invoice" aria-expanded="false" aria-controls="ui-invoice">
              <i class="ti-wallet menu-icon"></i>
              <span class="menu-title">Data Invoice</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-invoice">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">Data Baru</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/dropdowns.html">Invoice</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-urgent" aria-expanded="false" aria-controls="ui-urgent">
              <i class="ti-angle-double-up menu-icon"></i>
              <span class="menu-title">Data Urgent</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-urgent">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">SARAN</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/dropdowns.html">PENGADUAN</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/dropdowns.html">REQ. UP/DOWN GRADE</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-report" aria-expanded="false" aria-controls="ui-report">
              <i class="ti-files menu-icon"></i>
              <span class="menu-title">LAPORAN</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-report">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">BUKU BESAR</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/dropdowns.html">LAP. LABA RUGI</a></li>
              </ul>
            </div>
          </li>


        </ul>
      </nav>