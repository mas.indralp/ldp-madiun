<li class="nav-item nav-search d-none d-lg-block">
<div class="input-group">
    <div class="input-group-prepend hover-cursor" id="navbar-search-icon">
    <span class="input-group-text" id="search">
        <i class="icon-search"></i>
    </span>
    </div>
    <input type="text" class="form-control" id="navbar-search-input" placeholder="Search now" aria-label="search" aria-describedby="search">
</div>
</li>