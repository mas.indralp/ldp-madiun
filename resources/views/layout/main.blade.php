<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Skydash Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset("vendors/feather/feather.css") }}">
  <link rel="stylesheet" href="{{ asset("vendors/ti-icons/css/themify-icons.css")}}">
  <link rel="stylesheet" href="{{ asset("vendors/css/vendor.bundle.base.css")}}">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="{{ asset("vendors/select2/select2.min.css") }}">
  <link rel="stylesheet" href="{{ asset("vendors/select2-bootstrap-theme/select2-bootstrap.min.css") }}">
  <link rel="stylesheet" href="{{ asset("vendors/datatables.net-bs4/dataTables.bootstrap4.css") }}">
  <link rel="stylesheet" href="{{ asset("vendors/ti-icons/css/themify-icons.css") }}">
  <link rel="stylesheet" type="text/css" href="{{ asset("js/select.dataTables.min.css") }}">

  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ asset("css/vertical-layout-light/style.css") }}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset("images/favicon.png") }}" />
</head>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo mr-5" href="index.html"><img src="{{ asset("images/logo.svg") }}" class="mr-2" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="index.html"><img src="{{ asset("images/logo-mini.svg") }}" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="icon-menu"></span>
        </button>
        <ul class="navbar-nav mr-lg-2">
            @include('layout.header.search')
        </ul>
        <ul class="navbar-nav navbar-nav-right">
            @include('layout.header.notif')
            @include('layout.header.profile')
            @include('layout.header.setting')        
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="icon-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      @include('layout.theme-setting')
      @include('layout.sidebar.right-sidebar')
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      @include('layout.sidebar.left-sidebar')
      <!-- partial -->
      <div class="main-panel">
      <div class="content-wrapper">
        @yield('content')
      </div>
        <!-- content-wrapper ends -->
        @include('layout.footer')
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="{{ asset("vendors/js/vendor.bundle.base.js") }}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="{{ asset("vendors/typeahead.js/typeahead.bundle.min.js") }}"></script>
  <script src="{{ asset("vendors/select2/select2.min.js") }}"></script>
  <script src="{{ asset("vendors/chart.js/Chart.min.js") }}"></script>
  <script src="{{ asset("vendors/datatables.net/jquery.dataTables.js") }}"></script>
  <script src="{{ asset("vendors/datatables.net-bs4/dataTables.bootstrap4.js") }}"></script>
  <script src="{{ asset("js/dataTables.select.min.js") }}"></script>

  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="{{ asset("js/off-canvas.js") }}"></script>
  <script src="{{ asset("js/hoverable-collapse.js") }}"></script>
  <script src="{{ asset("js/template.js") }}"></script>
  <script src="{{ asset("js/settings.js") }}"></script>
  <script src="{{ asset("js/todolist.js") }}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{ asset("js/dashboard.js") }}"></script>
  <script src="{{ asset("js/Chart.roundedBarCharts.js") }}"></script>
  <script src="{{ asset("js/file-upload.js") }}"></script>
  <script src="{{ asset("js/typeahead.js") }}"></script>
  <script src="{{ asset("js/select2.js") }}"></script>
  <!-- End custom js for this page-->
</body>

</html>

