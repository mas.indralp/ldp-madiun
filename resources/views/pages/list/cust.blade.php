@extends('layout/main')
@section('content')

<div class="col-lg-12 grid-margin">
<div class="card">
    <div class="card-body">
    <h4 class="card-title">Daftar Pelanggan</h4>
    <p class="card-description">
        List Pelanggan <code>LDP</code>
    </p>
    <div class="table-responsive pt-3">
        <table class="table table-bordered">
        <thead>
            <tr>
                <th width="3%">No</th>
                <th>No Pelanggan</th>
                <th>Nama</th>
                <th>Perusahaan</th>
                <th>Telp</th>
                <th width="3%">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($dtcust as $index => $cust)
            <tr>
            <td>{{ $index +1 }}</td>
            <td><a href="{{ url('customer/edit/'.$cust->id) }}">{{ $cust->no_pelanggan }}</a></td>
            <td>{{ $cust->pj }}</td>
            <td>{{ $cust->perusahaan }}</td>           
            <td>{{ $cust->telp }}</td>           
            <td>
                <button type="button" class="btn btn-primary btn-icon" onclick="window.location='{{ url('customer/edit/'.$cust->id) }}'">
                    <i class="ti-pencil-alt"></i>
                </button>
                <button type="button" class="btn btn-danger btn-icon" onclick="window.location='{{ url('customer/del/'.$cust->id) }}'">
                    <i class="ti-close"></i>
                </button>
            </td>           
            </tr>
            @endforeach
        </tbody>
        </table>
    </div>
    </div>
</div>
</div>

@endsection