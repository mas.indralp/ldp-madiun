@extends('layout/main')
@section('content')

<div class="col-lg-12 grid-margin">
<div class="card">
    <div class="card-body">
    <h4 class="card-title">Daftar Installasi</h4>
    <p class="card-description">
        List Installasi <code>LDP</code>
    </p>
    <div class="table-responsive pt-3">
        <table class="table table-bordered">
        <thead>
            <tr>
                <th width="3%">No</th>
                <th>Id Installasi</th>
                <th>SN</th>
                <th>Nama</th>
                <th>Tgl Beli</th>
                <th>Status</th>
                <th width="3%">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $index => $dt)
            <tr>
            <td>{{ $index +1 }}</td>
            <td><a href="{{ url('inventory/edit/'.$dt->id_inventory) }}">{{ $dt->id_inventory }}</a></td>
            <td>{{ $dt->sn_inventory }}</td>
            <td>{{ $dt->nama }} </td>           
            <td>{{ $dt->tgl_beli }}</td>           
            <td>{{ $dt->status }}</td>           
            <td>
                <button type="button" class="btn btn-primary btn-icon" onclick="window.location='{{ url('inventory/edit/'.$dt->id_inventory) }}'">
                    <i class="ti-pencil-alt"></i>
                </button>
                <button type="button" class="btn btn-danger btn-icon" onclick="window.location='{{ url('inventory/del/'.$dt->id_inventory) }}'">
                    <i class="ti-close"></i>
                </button>
            </td>           
            </tr>
            @endforeach
        </tbody>
        </table>
    </div>
    </div>
</div>
</div>

@endsection