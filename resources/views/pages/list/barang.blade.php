@extends('layout/main')
@section('content')

<div class="col-lg-12 grid-margin">
<div class="card">
    <div class="card-body">
    <h4 class="card-title">Daftar Barang</h4>
    <p class="card-description">
        List barang <code>LDP</code>
    </p>
    <div class="table-responsive pt-3">
        <table class="table table-bordered">
        <thead>
            <tr>
                <th width="3%">No</th>
                <th>Kode Barang</th>
                <th>Nama Barang</th>
                <th>Jumlah</th>
                <th width="3%">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($dtbarang as $index => $brg)
            <tr>
            <td>{{ $index +1 }}</td>
            <td><a href="{{ url('barang/edit/'.$brg->id) }}">{{ $brg->kode }}</a></td>
            <td>{{ $brg->nama }}</td>
            <td>{{ $brg->jumlah }}</td>           
            <td>
                <button type="button" class="btn btn-primary btn-icon" onclick="window.location='{{ url('barang/edit/'.$brg->id) }}'">
                    <i class="ti-pencil-alt"></i>
                </button>
                <button type="button" class="btn btn-danger btn-icon" onclick="window.location='{{ url('barang/del/'.$brg->id) }}'">
                    <i class="ti-close"></i>
                </button>
            </td>           
            </tr>
            @endforeach
        </tbody>
        </table>
    </div>
    </div>
</div>
</div>

@endsection