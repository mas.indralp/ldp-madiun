@extends('layout/main')
@section('content')

<div class="col-lg-12 grid-margin">
<div class="card">
    <div class="card-body">
    <h4 class="card-title">Daftar Detail Order Alat</h4>
    <p class="card-description">
        List DO Alat <code>LDP</code>
    </p>
    <div class="table-responsive pt-3">
        <table class="table table-bordered">
        <thead>
            <tr>
                <th width="3%">No</th>
                <th>Id Order</th>
                <th>Inventory</th>
                <th>Tanggal</th>
                <th>Karyawan</th>
                <th width="3%">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $index => $dt)
            <tr>
            <td>{{ $index +1 }}</td>
            <td><a href="{{ url('doalat/edit/'.$dt->id_order) }}">{{ $dt->id_order }}</a></td>
            <td>{{ $dt->id_inventory }}</td>
            <td>{{ $dt->tgl_ambil }}</td>           
            <td>{{ $dt->id_karyawan }}</td>           
            <td>
                <button type="button" class="btn btn-primary btn-icon" onclick="window.location='{{ url('doalat/edit/'.$dt->id_order) }}'">
                    <i class="ti-pencil-alt"></i>
                </button>
                <button type="button" class="btn btn-danger btn-icon" onclick="window.location='{{ url('doalat/del/'.$dt->id_order) }}'">
                    <i class="ti-close"></i>
                </button>
            </td>           
            </tr>
            @endforeach
        </tbody>
        </table>
    </div>
    </div>
</div>
</div>

@endsection