@extends('layout/main')
@section('content')

<div class="col-lg-12 grid-margin">
<div class="card">
    <div class="card-body">
    <h4 class="card-title">Daftar Karyawan</h4>
    <p class="card-description">
        List Karyawan <code>LDP</code>
    </p>
    <div class="table-responsive pt-3">
        <table class="table table-bordered">
        <thead>
            <tr>
                <th width="3%">No</th>
                <th>NIP</th>
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Telp</th>
                <th width="3%">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($dtkary as $index => $kary)
            <tr>
            <td>{{ $index +1 }}</td>
            <td><a href="{{ url('karyawan/edit/'.$kary->id) }}">{{ $kary->no_pelanggan }}</a></td>
            <td>{{ $kary->pj }}</td>
            <td>{{ $kary->perusahaan }}</td>           
            <td>{{ $kary->telp }}</td>           
            <td>
                <button type="button" class="btn btn-primary btn-icon" onclick="window.location='{{ url('karyawan/edit/'.$kary->id) }}'">
                    <i class="ti-pencil-alt"></i>
                </button>
                <button type="button" class="btn btn-danger btn-icon" onclick="window.location='{{ url('karyawan/del/'.$kary->id) }}'">
                    <i class="ti-close"></i>
                </button>
            </td>           
            </tr>
            @endforeach
        </tbody>
        </table>
    </div>
    </div>
</div>
</div>

@endsection