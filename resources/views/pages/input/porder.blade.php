@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Purchase Order</h4>
        <form class="form-sample" action='/porder/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi Data Purchase Order
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Id PO</label>                
                    <input type="text" class="form-control" name='id' value="{{ @$post->id_po }}"/>                     
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal PO</label>
                    <input type="date" class="form-control" name='tgl' value="{{ @$post->tgl_po }}" />            
                </div>
            </div>                  
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal Tempo PO</label>
                    <input type="date" class="form-control" name='tgltempo' value="{{ @$post->tgl_tempo_po }}" />       
                </div>
            </div>   
                              
            <div class="col-md-6">
                <div class="form-group">
                    <label>Vendor</label>
                    <select class="js-example-basic-single w-100" name='vendor'>
                        @foreach ($vendor as $dt)
                            <option value="{{ $dt->id_vendor }}">{{ $dt->nama_vendor }}</option>
                        @endforeach      
                    </select>       
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tipe Bayar</label>
                    <select class="js-example-basic-single w-100" name='bayar'>	
                        <option value="transfer">TRANSFER</option>  
                        <option value="cash">CASH</option>  
                    </select>      
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Note</label>
                    <input type="text" class="form-control" name='note' value="{{ @$post->note }}" />       
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Status</label>
                    <input type="text" class="form-control" name='status' value="{{ @$post->status }}" />       
                </div>
            </div>          
                       
                 
                            
        </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection