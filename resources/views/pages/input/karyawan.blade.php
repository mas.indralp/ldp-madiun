@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Karyawan Baru</h4>
        <form class="form-sample" action='/karyawan/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi data karyawan
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Nomor Induk Karyawan</label>                
                    <input type="hidden" name="id" value="{{ @$kary->id }}">
                    <input type="text" class="form-control" name='nip' value="{{ @$kary->nip }}" placeholder="NIK"/>                
                </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label>Nama Karyawan</label>
                <input type="text" class="form-control" name='nama' value="{{ @$kary->nama }}" placeholder="Nama Lengkap"/>                
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label>Alamat</label>
                <textarea class="form-control" rows="4" name='alamat'>{{ @$kary->alamat }}</textarea>
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label>No. Telp</label>
                <input type="text" class="form-control" name='telp' value="{{ @$kary->telp }}"/>
            </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tempat Lahir</label>
                    <input type="text" class="form-control" name='tmptlhr' value="{{ @$kary->tempat_lhr }}"/>    
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="text" class="form-control" name='tgllhr' value="{{ @$kary->tgl_lhr }}"/>
    
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Jenis Kelamin</label>                    
                    <select class="form-control" name='jk'>
                        <option value="L">Laki-Laki</option>
                        <option value="P">Perempuan</option>                       
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Pendidikan Terakhir</label>                   
                    <select class="form-control" name='pendidikan'>
                        <option value="SD">SD</option>
                        <option value="SMP">SMP</option>                       
                        <option value="SMA">SMA/SMK</option>                       
                        <option value="S1">S1</option>                       
                        <option value="S2">S2</option>                       
                        <option value="S3">S3</option>                       
                    </select>
                </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label>No. NPWP</label>
                <input type="text" class="form-control" name='npwp' value="{{ @$kary->npwp }}"/>

            </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>No BPJS Tenaga Kerja</label>
                    <input type="text" class="form-control" name='bpjsker' value="{{ @$kary->bpjs_tker }}"/>
    
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tgl. Masuk</label>
                    <input type="text" class="form-control" name='tglmasuk' value="{{ @$kary->tglmasuk }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Status</label>
                    <select class="form-control" name='status'>
                        <option value="tetap">TETAP</option>
                        <option value="kontrak">KONTRAK</option>                       
                        <option value="magang">MAGANG</option>                       
                                           
                    </select>
                </div>
            </div>
            
        </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection