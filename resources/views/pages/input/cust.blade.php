@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Pelanggan Baru</h4>
        <form class="form-sample" action='/customer/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi data pelanggan
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Nomor Pelanggan</label>                
                    <input type="hidden" name="id" value="{{ @$cust->id }}">
                    <input type="text" class="form-control" name='nopel' value="{{ @$cust->no_pelanggan }}"/>                
                </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label>Nama Pelanggan</label>
                <input type="text" class="form-control" name='namapel' value="{{ @$cust->pj }}"/>                
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label>NIK</label>
                <input type="text" class="form-control" name='nik' value="{{ @$cust->nik }}"/>

            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label>Jabatan</label>
                <input type="text" class="form-control" name='jabatan' value="{{ @$cust->jabatan }}"/>

            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label>Perusahaan</label>
                <input type="text" class="form-control" name='perusahaan' value="{{ @$cust->perusahaan }}"/>

            </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" class="form-control" name='alamat' value="{{ @$cust->alamat }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kode Pos</label>
                    <input type="text" class="form-control" name='kodepos' value="{{ @$cust->kodepos }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>No. Telp/HP</label>
                    <input type="text" class="form-control" name='telp' value="{{ @$cust->telp }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" class="form-control" name='email' value="{{ @$cust->email }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Koordinat Lokasi</label>
                    <input type="text" class="form-control" name='koordinat' value="{{ @$cust->coordinat }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Technical Contact</label>
                    <input type="text" class="form-control" name='tech_ct' value="{{ @$cust->tech_ctc }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Technical Telp</label>
                    <input type="text" class="form-control" name='tech_telp' value="{{ @$cust->tech_telp }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Technical Email</label>
                    <input type="text" class="form-control" name='tech_email' value="{{ @$cust->tech_email }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Finance Contact</label>
                    <input type="text" class="form-control" name='fin_ct' value="{{ @$cust->fint_ctc }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Finance Telp</label>
                    <input type="text" class="form-control" name='fin_telp' value="{{ @$cust->fin_telp }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Fincance Email</label>
                    <input type="text" class="form-control" name='fin_email' value="{{ @$cust->fin_email }}"/>
                </div>
            </div>
        </div>
        <p class="card-description">
            Jenis Pelayanan
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Layanan</label>
                    
                    <select class="form-control" name='layanan' value="{{ @$cust->layanan }}">
                        <option>Koneksi Internet</option>
                        <option>CO-LACATION</option>
                        <option>Koneksi IIX</option>
                        <option>VPN</option>
                        <option>VPN-VoIP</option>
                        <option>Software</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Port Bandwitch</label>
                    <input type="text" class="form-control" name='port_bnwt' value="{{ @$cust->port_bndwt }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Media</label>
                    <input type="text" class="form-control" name='media' value="{{ @$cust->media }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Media Oleh</label>                    
                    <select class="form-control" name='mediaoleh' value="{{ @$cust->media_oleh }}">
                        <option>FIBER OPTIC</option>
                        <option>WIRELESS</option>                       
                    </select>
                </div>
            </div>
        </div>
            <p class="card-description">
                Data Finance
            </p>
            <div class="row">
    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Nomor NPWP</label>
                    <input type="text" class="form-control" name='nonpwp' value="{{ @$cust->npwp }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Alamat NPWP</label>
                    <input type="text" class="form-control" name='alamatnpwp' value="{{ @$cust->alamat_npwp }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Jenis Setoran</label>                    
                    <select class="form-control" name='setoran' value="{{ @$cust->jns_setoran }}">
                        <option>TRANSFER BANK</option>
                        <option>TUNAI</option>                       
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Biaya Bulanan</label>
                    <input type="text" class="form-control" name='biaya' value="{{ @$cust->biaya_bulanan }}"/>
                </div>
            </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection