@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Layanan</h4>
        <form class="form-sample" action='/layanan/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi Data Layanan
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Nama Layanan</label>                
                    <input type="hidden" class="form-control" name='id' value="{{ @$post->id_layanan }}"/>  
                    <input type="text" class="form-control" name='nama' value="{{ @$post->nama_layanan }}"/>  
                              
                </div>
            </div>           
            <div class="col-md-6">
            <div class="form-group">
                <label>Diskripsi Layanan</label>
                <input type="text" class="form-control" name='diskripsi' value="{{ @$post->desk_layanan }}" />            
            </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Harga Layanan</label>
                    <input type="text" class="form-control" name='harga' value="{{ @$post->harga_layanan }}" />       
                </div>
            </div>           
                            
        </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection