@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data User</h4>
        <form class="form-sample" action='/user/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi Data User
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >User name</label>                
                    <input type="hidden" class="form-control" name='id' value="{{ @$post->user_id }}"/>    
                    <input type="text" class="form-control" name='name' value="{{ @$post->user_name }}"/>                     
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name='email1' value="{{ @$post->user_email }}"/>            
                </div>
            </div>                  
            <div class="col-md-6">
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name='pass' value="{{ @$post->user_password }}" />       
                </div>
            </div>   
                              
            <div class="col-md-6">
                <div class="form-group">
                    <label>User Role</label>
                    <input type="text" class="form-control" name='role' value="{{ @$post->user_role }}" />          
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Id Reff</label>
                    <input type="text" class="form-control" name='reff' value="{{ @$post->id_reff }}" />     
                </div>
            </div>          
            <div class="col-md-6">
                <div class="form-group">
                    <label>Email Validation</label>
                    <input type="email" class="form-control" name='email2' value="{{ @$post->email_validate_at }}" />     
                </div>
            </div>             
            
            <div class="col-md-6">
                <div class="form-group">
                    <label>Status</label>
                    <input type="text" class="form-control" name='status' value="{{ @$post->status }}" />       
                </div>
            </div>   
                       
                 
                            
        </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection