@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
                <h3>Form Pengisian Logistik Masuk</h3>
                <form class="form-sample" action='/logistik/save' method='post' enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Tanggal</label>                
                            <input type="hidden" class="form-control" name='id' value="{{ @$post->id }}"/>  
                            <input type="date" class="form-control" name='tgl' value="{{ @$post->tanggal }}"/>  
                                      
                        </div>
                    </div>           
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Perangkat</label>
                            <input type="text" class="form-control" name='perangkat' value="{{ @$post->perangkat }}" />            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tipe</label>
                            <input type="text" class="form-control" name='tipe' value="{{ @$post->tipe }}" />       
                        </div>
                    </div>           
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Jumlah</label>
                            <input type="text" class="form-control" name='jumlah' value="{{ @$post->jummlah }}" />       
                        </div>
                    </div>           
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Alokasi</label>
                            <input type="text" class="form-control" name='alokasi' value="{{ @$post->alokasi }}" />       
                        </div>
                    </div>           
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Pelanggan</label>
                            <select class="js-example-basic-single w-100" name='pelanggan'>
                                @foreach ($cust as $dt)
                                <option value="{{ $dt->id_pelanggan }}">{{ $dt->nama_pelanggan }}</option>
                                @endforeach      
                            </select>       
                        </div>
                    </div>           
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Karyawan</label>
                            <select class="js-example-basic-single w-100" name='kary'>
                                @foreach ($kary as $dt)
                                <option value="{{ $dt->id }}">{{ $dt->nama }}</option>
                                @endforeach      
                            </select>        
                        </div>
                    </div>           
                                    
                </div>

                    <button type="submit" class="btn btn-primary mr-2">Add</button>
                    <button class="btn btn-light">Cancel</button>              
            
            </form>
            
            <table class="table table-bordered">
                <tr class="success"><th colspan="6">Detail Logistik Masuk Nomor : xx-xx-xxx </th></tr>
                <tr>
                    <th>No</th> <th>Tanggal</th><th>Perangkat</th><th>Tipe</th><th>Qty</th><th>Cancel</th></tr>
                        <tr>
                        <td>1</td>
                        <td width=''>Tanggal</td>
                        <td width=''>Perangkat</td>
                        <td width=''>Tipe</td>
                        <td width=''>Qty</td>
                        <td><button type="submit" class="btn btn-danger">x</button> <a href="">Detail</a></td>
                    </tr>
                        
            </table>
        </div>
    </div>
</div>


@endsection