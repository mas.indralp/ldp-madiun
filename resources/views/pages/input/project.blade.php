@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Project</h4>
        <form class="form-sample" action='/project/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi Data Project
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Id Project</label>                
                    <input type="text" class="form-control" name='id' value="{{ @$post->id_project }}"/>                     
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal</label>
                    <input type="date" class="form-control" name='tgl' value="{{ @$post->tgl_project }}" />            
                </div>
            </div>                  
            <div class="col-md-6">
                <div class="form-group">
                    <label>Nama Project</label>
                    <input type="text" class="form-control" name='nama' value="{{ @$post->nm_project }}" />       
                </div>
            </div>   
                              
            <div class="col-md-6">
                <div class="form-group">
                    <label>Diskripsi</label>
                    <input type="text" class="form-control" name='diskripsi' value="{{ @$post->desk_project }}" />       
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Pelanggan</label>
                    <select class="js-example-basic-single w-100" name='idpel'>
                        @foreach ($cust as $dt)
                            <option value="{{ $dt->id_pelanggan }}">{{ $dt->nama_pelanggan }}</option>
                        @endforeach      
                    </select>      
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Owner Project</label>
                    <input type="text" class="form-control" name='owner' value="{{ @$post->owner_project }}" />       
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Status</label>
                    <input type="text" class="form-control" name='status' value="{{ @$post->status }}" />       
                </div>
            </div>          
                       
                 
                            
        </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection