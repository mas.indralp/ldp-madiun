@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Detail Order Layanan</h4>
        <form class="form-sample" action='/dolayanan/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi Detail Order Layanan
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >ID Order</label>                
                    <input type="text" class="form-control" name='id' value="{{ @$post->id_order }}"/>                
                </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label>Layanan</label>
                <select class="js-example-basic-single w-100" name='idlayanan'>
                    @foreach ($layanan as $lay)
                    <option value="{{ $lay->id_layanan }}">{{ $lay->nama_layanan }}</option>
                    @endforeach      
                </select>            
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label>Harga Layanan</label>
                <input type="text" class="form-control" name='harga' value="{{ @$post->harga_layanan }}" />       
            </div>
            </div>
                                
        </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection