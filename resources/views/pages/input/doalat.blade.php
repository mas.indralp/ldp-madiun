@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
                <h3>Form Pengisian</h3>
                <form class="form-sample" action='/doalat/save' method='post' enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >ID Order</label>                
                            <input type="text" class="form-control" name='id' value="{{ @$post->nip }}"/>                
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Inventory</label>
                            <select class="js-example-basic-single w-100" name='idinven'>
                                @foreach ($inven as $inventory)
                                <option value="{{ $inventory->id_inventory }}">{{ $inventory->nama }}</option>
                                @endforeach      
                            </select>            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tanggal Pengambilan</label>
                            <input type="date" class="form-control" name='tgl' value="{{ @$post->nama }}" />       
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Karyawan</label>
                            <select class="js-example-basic-single w-100" name='idkary'>
                                @foreach ($karyawan as $id)
                                <option value="{{ $id->nik }}">{{ $id->nama }}</option>
                                @endforeach    
                            </select>
                        </div>                                   
                    </div>
                </div>

                    <button type="submit" class="btn btn-primary mr-2">Add</button>
                    <button class="btn btn-light">Cancel</button>              
            
            </form>
            
            <table class="table table-bordered">
                <tr class="success"><th colspan="6">Detail Inventory</th></tr>
                <tr>
                    <th>No</th> <th>Inventory</th><th>Cancel</th></tr>
                        <tr>
                        <td>1</td>
                        <td width='80%'>inventory</td>
                        <td><button type="submit" class="btn btn-danger">Cancel</button></td>
                    </tr>
                        
            </table>
        </div>
    </div>
</div>


@endsection