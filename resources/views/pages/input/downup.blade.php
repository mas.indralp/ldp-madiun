@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
                <h3>Form Pengisian</h3>
                <form class="form-sample" action='/logistik/save' method='post' enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >No Pelanggan</label>                
                            <input type="text" class="form-control" name='id' value="{{ @$post->id }}"/>  
                                 
                        </div>
                    </div>    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Tanggal</label> 
                                <input type="date" class="form-control" name='tgl' value="{{ @$post->tanggal }}"/>  
                                      
                        </div>
                    </div>          
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Nama Layanan</label>                
                            <input type="hidden" class="form-control" name='id' value="{{ @$post->id_layanan }}"/>  
                            <input type="text" class="form-control" name='nama' value="{{ @$post->nama_layanan }}"/>  
                                      
                        </div>
                    </div>
                            
                                    
                </div>

                    <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                    <button class="btn btn-light">Cancel</button>              
            
            </form>
            
            
        </div>
    </div>
</div>


@endsection