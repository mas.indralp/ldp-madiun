@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Order</h4>
        <form class="form-sample" action='/order/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi Data Order
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >No Order</label>                
                    <input type="hidden" class="form-control" name='id' value="{{ @$post->id_order }}"/>  
                    <input type="text" class="form-control" name='noorder' value="{{ @$post->no_order }}"/>  
                    
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal Order</label>
                    <input type="date" class="form-control" name='tgl1' value="{{ @$post->tgl_order }}" />            
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Pelanggan</label>
                    <select class="js-example-basic-single w-100" name='idpel'>
                        @foreach ($cust as $dt)
                            <option value="{{ $dt->id_pelanggan }}">{{ $dt->nama_pelanggan }}</option>
                        @endforeach      
                    </select>       
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Media</label>
                    <input type="text" class="form-control" name='media' value="{{ @$post->media }}" />       
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Note</label>
                    <input type="text" class="form-control" name='note' value="{{ @$post->note }}" />       
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal Installasi</label>
                    <input type="date" class="form-control" name='tglins' value="{{ @$post->tgl_installasi }}" />       
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal Aktivasi</label>
                    <input type="date" class="form-control" name='tglakt' value="{{ @$post->tgl_aktivasi }}" />       
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Total</label>
                    <input type="text" class="form-control" name='total' value="{{ @$post->total }}" />       
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Status</label>
                    <input type="text" class="form-control" name='status' value="{{ @$post->Status }}" />       
                </div>
            </div>           
                 
                            
        </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection