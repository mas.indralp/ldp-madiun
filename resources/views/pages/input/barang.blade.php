@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Barang Baru</h4>
        <form class="form-sample" action='/barang/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi data barang
        </p>
        <div class="row">
            <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Kode Barang</label>
                <div class="col-sm-9">
                <input type="hidden" name="id" value="{{ @$brg->id }}">
                <input type="text" class="form-control" name='kodebrg' value="{{ @$brg->kode }}"/>
                </div>
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nama Barang</label>
                <div class="col-sm-9">
                <input type="text" class="form-control" name='namabrg' value="{{ @$brg->nama }}"/>
                </div>
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Jumlah</label>
                <div class="col-sm-9">
                <input type="text" class="form-control" name='jumlahbrg' value="{{ @$brg->jumlah }}"/>
                </div>
            </div>
            </div>
        </div>
        
        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection