@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Pelanggan Baru</h4>
        <form class="form-sample" action='/customer_1/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi data pelanggan
        </p>
        <div class="row">
            
            <div class="col-md-6">
            <div class="form-group">
                <label>Nama Pelanggan</label>
                <input type="text" class="form-control" name='namapel' value="{{ @$cust->pj }}"/>                
            </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>NIK</label>
                    <input type="text" class="form-control" name='nik' value="{{ @$cust->nik }}"/>

                </div>
            </div>            
          
            <div class="col-md-6">
                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" class="form-control" name='alamat' value="{{ @$cust->alamat }}"/>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="form-group">
                    <label>No. Telp/HP</label>
                    <input type="text" class="form-control" name='telp' value="{{ @$cust->telp }}"/>
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Koordinat Lokasi</label>
                    <input type="text" class="form-control" name='koordinat' value="{{ @$cust->coordinat }}"/>
                </div>
            </div>            
        </div>
        <p class="card-description">
            Jenis Pelayanan
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Layanan</label>
                    
                    <select class="form-control" name='layanan' value="{{ @$cust->layanan }}">
                        <option>Koneksi Internet</option>
                        <option>CO-LACATION</option>
                        <option>Koneksi IIX</option>
                        <option>VPN</option>
                        <option>VPN-VoIP</option>
                        <option>Software</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Port Bandwitch</label>
                    <input type="text" class="form-control" name='port_bnwt' value="{{ @$cust->port_bndwt }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Media</label>
                    <input type="text" class="form-control" name='media' value="{{ @$cust->media }}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Media Oleh</label>                    
                    <select class="form-control" name='mediaoleh' value="{{ @$cust->media_oleh }}">
                        <option value="fiber">FIBER OPTIC</option>
                        <option value="wireless">WIRELESS</option>                       
                    </select>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>Progress</label>                    
                    <select class="form-control" name='progress'>
                        <option value="progress">PROGRESS</option>
                        <option value="clear">CLEAR</option>                       
                        <option value="cancel">CANCEL</option>                       
                    </select>
                </div>
            </div>
        </div>
           
        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection