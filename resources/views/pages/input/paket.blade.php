@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Paket</h4>
        <form class="form-sample" action='/paket/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi Data Paket
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Layanan</label>                
                    <input type="hidden" class="form-control" name='id' value="{{ @$post->id_paket }}"/>  
                    <input type="text" class="form-control" name='idlayanan' value="{{ @$post->id_layanan }}"/>  
                    
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kecepatan</label>
                    <input type="text" class="form-control" name='kecepatan' value="{{ @$post->kecepatan }}" />            
                </div>
            </div>
                  
            <div class="col-md-6">
                <div class="form-group">
                    <label>Lain-Lain</label>
                    <input type="text" class="form-control" name='other' value="{{ @$post->other }}" />       
                </div>
            </div>           
                       
                 
                            
        </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection