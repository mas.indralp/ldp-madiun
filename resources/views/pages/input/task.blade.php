@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Task</h4>
        <form class="form-sample" action='/task/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi Data Task
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Id Task</label>                
                    <input type="text" class="form-control" name='id' value="{{ @$post->id_task }}"/>                     
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Project</label>
                    <select class="js-example-basic-single w-100" name='project'>
                        @foreach ($project as $dt)
                            <option value="{{ $dt->id_projectn }}">{{ $dt->nm_project }}</option>
                        @endforeach      
                    </select>            
                </div>
            </div>                  
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal Mulai</label>
                    <input type="date" class="form-control" name='tgl2' value="{{ @$post->tgl_start }}" />       
                </div>
            </div>   
                              
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal Selesai</label>
                    <input type="date" class="form-control" name='tgl2' value="{{ @$post->tgl_end }}" />          
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Nama Task</label>
                    <input type="text" class="form-control" name='nama' value="{{ @$post->nm_task }}" />     
                </div>
            </div>          
            <div class="col-md-6">
                <div class="form-group">
                    <label>Diskripsi Task</label>
                    <input type="text" class="form-control" name='diskripsi' value="{{ @$post->desk_task }}" />     
                </div>
            </div>             
            <div class="col-md-6">
                <div class="form-group">
                    <label>Karyawan</label>
                    <select class="js-example-basic-single w-100" name='kary'>
                        @foreach ($kary as $dt)
                            <option value="{{ $dt->id_karyawan }}">{{ $dt->nama }}</option>
                        @endforeach      
                    </select>       
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Prioritas</label>
                    <select class="js-example-basic-single w-100" name='prioritas'>	
                        <option value="high">TINGGI</option>  
                        <option value="middle">SEDANG</option>  
                        <option value="low">RENDAH</option>  
                    </select>       
                </div>
            </div>   
            <div class="col-md-6">
                <div class="form-group">
                    <label>Jenis</label>
                    <input type="text" class="form-control" name='jenis' value="{{ @$post->jenis }}" />       
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Status</label>
                    <input type="text" class="form-control" name='status' value="{{ @$post->status }}" />       
                </div>
            </div>   
                       
                 
                            
        </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection