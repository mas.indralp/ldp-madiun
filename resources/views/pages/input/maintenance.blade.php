@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Maintenance</h4>
        <form class="form-sample" action='/maintenance/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi Data Maintenance
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Pelanggan</label>                
                    <input type="hidden" class="form-control" name='id' value="{{ @$post->id_maintenance }}"/>  
                    <select class="js-example-basic-single w-100" name='idpel'>
                        @foreach ($cust as $dt)
                        <option value="{{ $dt->id_pelanggan }}">{{ $dt->nama_pelanggan }}</option>
                        @endforeach      
                    </select> 
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal Mulai</label>
                    <input type="date" class="form-control" name='tgl1' value="{{ @$post->tgl_mulai }}" />            
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal Selesai</label>
                    <input type="date" class="form-control" name='tgl2' value="{{ @$post->tgl_selesai }}" />       
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Keluhan Pelanggan</label>
                    <input type="text" class="form-control" name='keluhan' value="{{ @$post->keluhan }}" />       
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Solusi</label>
                    <input type="text" class="form-control" name='solusi' value="{{ @$post->solusi }}" />       
                </div>
            </div>           
                 
                            
        </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection