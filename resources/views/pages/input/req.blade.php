@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Request Inventory</h4>
        <form class="form-sample" action='/porder/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi Data Request Inventory
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Id Request Inventory</label>                
                    <input type="text" class="form-control" name='id' value="{{ @$post->id_req_order }}"/>                     
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal Request</label>
                    <input type="date" class="form-control" name='tgl' value="{{ @$post->tgl_req }}" />            
                </div>
            </div>                  
            <div class="col-md-6">
                <div class="form-group">
                    <label>Inventory</label>
                    <input type="text" class="form-control" name='inventory' value="{{ @$post->id_inventory }}" />       
                </div>
            </div>   
                              
            <div class="col-md-6">
                <div class="form-group">
                    <label>Nama Barang</label>
                    <input type="text" class="form-control" name='nama' value="{{ @$post->nama_barang }}" />         
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Harga</label>
                    <input type="text" class="form-control" name='harga' value="{{ @$post->harga }}" />     
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Jumlah</label>
                    <input type="text" class="form-control" name='jumlah' value="{{ @$post->jumlah }}" />       
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Karyawan</label>
                    <input type="text" class="form-control" name='karyawan' value="{{ @$post->id_karyawan }}" />       
                </div>
            </div>   
            <div class="col-md-6">
                <div class="form-group">
                    <label>Status</label>
                    <input type="text" class="form-control" name='status' value="{{ @$post->status }}" />       
                </div>
            </div>       
                       
                 
                            
        </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection