@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
                <h3>Form Pengisian</h3>
                <form class="form-sample" action='/porder/save' method='post' enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >ID Purchase Order</label>                
                            <input type="text" class="form-control" name='idorder' value="{{ @$post->id_po }}"/>                
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Inventory</label>  
                            <select class="js-example-basic-single w-100" name='idinven'>
                                @foreach ($inven as $in)
                                <option value="{{ $in->id_inventory }}">{{ $in->nama }}</option>
                                @endforeach      
                            </select>               
                                         
                        </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <label>Harga</label>
                        <input type="text" class="form-control" name='harga' value="{{ @$post->harga }}"/>             
                    </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Jumlah Order</label>
                            <input type="text" class="form-control" name='jmlorder' value="{{ @$post->jml_order }}" />       
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Jumlah Datang</label>
                            <input type="text" class="form-control" name='jmldatang' value="{{ @$post->jml_datang }}" />       
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Alokasi</label>
                            <input type="text" class="form-control" name='alokasi' value="{{ @$post->alokasi }}" />       
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Diskripsi</label>
                            <input type="text" class="form-control" name='diskripsi' value="{{ @$post->desk_dpo }}" />       
                        </div>
                    </div>
                                        
                </div>

                    <button type="submit" class="btn btn-primary mr-2">Add</button>
                    <button class="btn btn-light">Cancel</button>              
            
            </form>
            
            <table class="table table-bordered">
                <tr class="success"><th colspan="8">Detail Transaksi</th></tr>
                <tr>
                    <th>No</th><th>Inventory</th><th>Qty Order</th><th>Qty Datang</th> <th>Harga</th><th>Alokasi</th><th>Subtotal</th><th>Action</th></tr>
                      <tr>
                        <td>1</td>
                        <td>nama_tiket</td>
                        <td>qty order</td>
                        <td>qty datang</td>
                        <td>harga</td>
                        <td>alokasi</td>
                        <td>total</td>
                        
                        <td><button type="submit" class="btn btn-danger">X</button></td></tr>
                        <tr>
                            <td colspan="6"><p align="right">Total</p></td>
                            <td colspan="2">total</td></tr>
            </table>
        </div>
    </div>
</div>
@endsection

