@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Inventory</h4>
        <form class="form-sample" action='/inventory/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi Data Inventory
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Serial Number Inventory</label>                
                    <input type="hidden" class="form-control" name='id' value="{{ @$post->id_inventory }}"/>  
                    <input type="text" class="form-control" name='sn' value="{{ @$post->sn_inventory }}"/>  
                              
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label >Nama Inventory</label>  
                    <input type="text" class="form-control" name='nama' value="{{ @$post->nama }}"/>  
                </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label>Vendor</label>
                <select class="js-example-basic-single w-100" name='idlayanan'>
                    @foreach ($vendor as $dt)
                    <option value="{{ $dt->id_vendor }}">{{ $dt->nama_vendor }}</option>
                    @endforeach      
                </select>            
            </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal Beli</label>
                    <input type="date" class="form-control" name='tgl' value="{{ @$post->tgl_beli }}" />       
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Harga Beli</label>
                    <input type="text" class="form-control" name='harga' value="{{ @$post->harga_beli }}" />       
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Diskripsi inventory</label>
                    <input type="text" class="form-control" name='diskripsi' value="{{ @$post->desk_inventory }}" />       
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kategori Inventory</label>
                    <select class="js-example-basic-single w-100" name='kategori'>	
                        <option value="project">Project</option>  
                        <option value="rumahan">Rumahan</option>  
                        <option value="pribadi">Pribadi</option>  
                    </select>       
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kondisi</label>
                    <input type="text" class="form-control" name='kondisi' value="{{ @$post->kondisi }}" />       
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Status</label>
                    <input type="text" class="form-control" name='status' value="{{ @$post->status }}" />     
                </div>
            </div>
                            
        </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection