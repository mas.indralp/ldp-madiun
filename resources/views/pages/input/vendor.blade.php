@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Vendor</h4>
        <form class="form-sample" action='/vendor/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi Data Vendor
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Nama Vendor</label>                
                    <input type="hidden" class="form-control" name='id' value="{{ @$post->id_vendor }}"/>    
                    <input type="text" class="form-control" name='nama' value="{{ @$post->nama_vendor }}"/>                     
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" class="form-control" name='alamat' value="{{ @$post->alamat }}"/>            
                </div>
            </div>                  
            <div class="col-md-6">
                <div class="form-group">
                    <label>Telp</label>
                    <input type="text" class="form-control" name='telp' value="{{ @$post->telp }}" />       
                </div>
            </div>   
                              
            <div class="col-md-6">
                <div class="form-group">
                    <label>Owner</label>
                    <input type="text" class="form-control" name='owner' value="{{ @$post->owner }}" />          
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Diskripsi</label>
                    <input type="text" class="form-control" name='diskripsi' value="{{ @$post->deskripsi }}" />     
                </div>
            </div>          
                                        
        </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection