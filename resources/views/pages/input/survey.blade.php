@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Survey</h4>
        <form class="form-sample" action='/survey/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi Data Survey
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Id Survey</label>                
                    <input type="text" class="form-control" name='id' value="{{ @$post->id_req_order }}"/>                     
                </div>
            </div>           
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal Mulai</label>
                    <input type="date" class="form-control" name='tgl1' value="{{ @$post->tgl_mulai }}" />            
                </div>
            </div>                  
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal Selesai</label>
                    <input type="date" class="form-control" name='tgl2' value="{{ @$post->tgl_selesai }}" />       
                </div>
            </div>   
                              
            <div class="col-md-6">
                <div class="form-group">
                    <label>Pelanggan</label>
                    <select class="js-example-basic-single w-100" name='pelanggan'>
                        @foreach ($cust as $dt)
                            <option value="{{ $dt->id_pelanggan }}">{{ $dt->nama_pelanggan }}</option>
                        @endforeach      
                    </select>        
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Location</label>
                    <input type="text" class="form-control" name='geo' value="{{ @$post->geo }}" />     
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Layanan</label>
                    <select class="js-example-basic-single w-100" name='layanan'>
                        @foreach ($layanan as $dt)
                            <option value="{{ $dt->id_layanan }}">{{ $dt->nama_layanan }}</option>
                        @endforeach      
                    </select>       
                </div>
            </div>                    
            <div class="col-md-6">
                <div class="form-group">
                    <label>Paket</label>
                    <input type="text" class="form-control" name='paket' value="{{ @$post->paket }}" />       
                </div>
            </div>   
            <div class="col-md-6">
                <div class="form-group">
                    <label>Ketarangan</label>
                    <input type="text" class="form-control" name='ket' value="{{ @$post->keterangan }}" />       
                </div>
            </div>      
            <div class="col-md-6">
                <div class="form-group">
                    <label>RFS</label>
                    <input type="text" class="form-control" name='rfs' value="{{ @$post->rfs }}" />       
                </div>
            </div>  <div class="col-md-6">
                <div class="form-group">
                    <label>Status</label>
                    <input type="text" class="form-control" name='status' value="{{ @$post->status }}" />       
                </div>
            </div>   
                       
                 
                            
        </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection