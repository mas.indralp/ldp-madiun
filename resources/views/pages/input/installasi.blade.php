@extends('layout.main')
@section('content')
<div class="col-12 grid-margin">
    <div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Instalasi Layanan</h4>
        <form class="form-sample" action='/installasi/save' method='post' enctype="multipart/form-data">
        @csrf
        <p class="card-description">
           Isi Instalasi Layanan
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >ID Order</label>                
                    <input type="hidden" class="form-control" name='id' value="{{ @$post->id_instalasi }}"/>  
                    <select class="js-example-basic-single w-100" name='idorder'>
                        @foreach ($order as $dt)
                        <option value="{{ $dt->id_order }}">{{ $dt->no_order }}</option>
                        @endforeach      
                    </select>              
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label >Tanggal Mulai</label>  
                    <input type="date" class="form-control" name='tglmulai' value="{{ @$post->tgl_mulai }}"/>  
                </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label>Tanggal Selesai</label>
                <input type="date" class="form-control" name='tglselesai' value="{{ @$post->tgl_selesai }}"/>             
            </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Layanan</label>
                    <input type="text" class="form-control" name='layanan' value="{{ @$post->layanan }}" />       
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Interface</label>
                    <input type="text" class="form-control" name='interface' value="{{ @$post->interface }}" />       
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>LO</label>
                    <input type="text" class="form-control" name='lo' value="{{ @$post->lo }}" />       
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>LT</label>
                    <input type="text" class="form-control" name='lt' value="{{ @$post->lt }}" />       
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Perangkat</label>
                    <input type="text" class="form-control" name='perangkat' value="{{ @$post->perangkat }}" />       
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Bukti</label>
                    <input type="file" name="img[]" class="file-upload-default" name="bukti">
                    <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                    </div>     
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Status</label>
                    <select class="js-example-basic-single w-100" name='status'>
                        <option value="Pending">PENDING</option>
                        <option value="Progress">PROGRESS</option>
                        <option value="Finish">FINISH</option>
                    </select>     
                </div>
            </div>                 
        </div>

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <button class="btn btn-light">Cancel</button>
        </form>

    </div>
    </div>
</div>

@endsection