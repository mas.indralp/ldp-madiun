<?php

use App\Models\inputbarang;
use App\Http\Controllers\Task_Ctrl;
use App\Http\Controllers\User_Ctrl;
use App\Http\Controllers\Order_Ctrl;
use App\Http\Controllers\Paket_Ctrl;
use App\Http\Controllers\Proyek_Ctrl;
use App\Http\Controllers\Survey_Ctrl;
use App\Http\Controllers\Vendor_Ctrl;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Layanan_Ctrl;
use App\Http\Controllers\Request_Ctrl;
use App\Http\Controllers\Customer_Ctrl;
use App\Http\Controllers\Karyawan_Ctrl;
use App\Http\Controllers\Logistik_Ctrl;
use App\Http\Controllers\Inventory_Ctrl;
use App\Http\Controllers\Installasi_Ctrl;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\Maintenance_Ctrl;
use App\Http\Controllers\DetailPOrder_Ctrl;
use App\Http\Controllers\PurchaseOrder_Ctrl;
use App\Http\Controllers\DetailOrderAlat_Ctrl;
use App\Http\Controllers\DetailOrderLayanan_Ctrl;
use App\Http\Controllers\DownUp_Ctrl;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/home', function () {
    return view('home',[
        "nama" => "indra",
        "Telp" => "081335808100"
    ]);
});
/*
|--------------------------------------------------------------------------
| Customer Tahap 1
|--------------------------------------------------------------------------
*/
Route::get('/customer_1',[CustomerController::class,'index_1']);
Route::post('/customer_1/save',[CustomerController::class,'simpan_1']);
Route::get('/listcustomer_1',[CustomerController::class,'list_1']);
Route::get('/customer_1/edit/{id}',[CustomerController::class,'edit_1']);
Route::get('/customer_1/del/{id}',[CustomerController::class,'delete_1']);

/*
|--------------------------------------------------------------------------
| Customer
|--------------------------------------------------------------------------
*/
Route::get('/customer',[CustomerController::class,'index']);
Route::post('/customer/save',[CustomerController::class,'simpan']);
Route::get('/listcustomer',[CustomerController::class,'list']);
Route::get('/customer/edit/{id}',[CustomerController::class,'edit']);
Route::get('/customer/del/{id}',[CustomerController::class,'delete']);

/*
|--------------------------------------------------------------------------
| Barang
|--------------------------------------------------------------------------
*/
Route::get('/barang',[BarangController::class,'index']);
Route::post('/barang/save',[BarangController::class,'simpan']);
Route::get('/listbarang',[BarangController::class,'list']);
Route::get('/barang/edit/{id}',[BarangController::class,'edit']);
Route::get('/barang/del/{id}',[BarangController::class,'delete']);

/*
|--------------------------------------------------------------------------
| Karyawan
|--------------------------------------------------------------------------
*/
Route::get('/karyawan',[Karyawan_Ctrl::class,'index']);
Route::post('/karyawan/save',[Karyawan_Ctrl::class,'simpan']);
Route::get('/listkaryawan',[Karyawan_Ctrl::class,'list']);
Route::get('/karyawan/edit/{id}',[Karyawan_Ctrl::class,'edit']);
Route::get('/karyawan/del/{id}',[Karyawan_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Logistik
|--------------------------------------------------------------------------
*/
Route::get('/logistik',[Logistik_Ctrl::class,'index']);
Route::get('/logistikout',[Logistik_Ctrl::class,'indexout']);
Route::post('/logistik/save',[Logistik_Ctrl::class,'simpan']);
Route::get('/listlogistik',[Logistik_Ctrl::class,'list']);
Route::get('/logistik/edit/{id}',[Logistik_Ctrl::class,'edit']);
Route::get('/logistik/del/{id}',[Logistik_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Detail Order Alat
|--------------------------------------------------------------------------
*/
Route::get('/doalat',[DetailOrderAlat_Ctrl::class,'index']);
Route::post('/doalat/save',[DetailOrderAlat_Ctrl::class,'simpan']);
Route::get('/listdoalat',[DetailOrderAlat_Ctrl::class,'list']);
Route::get('/doalat/edit/{id}',[DetailOrderAlat_Ctrl::class,'edit']);
Route::get('/doalat/del/{id}',[DetailOrderAlat_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Detail Order Layanan
|--------------------------------------------------------------------------
*/
Route::get('/dolayanan',[DetailOrderLayanan_Ctrl::class,'index']);
Route::post('/dolayanan/save',[DetailOrderLayanan_Ctrl::class,'simpan']);
Route::get('/listdolayanan',[DetailOrderLayanan_Ctrl::class,'list']);
Route::get('/dolayanan/edit/{id}',[DetailOrderLayanan_Ctrl::class,'edit']);
Route::get('/dolayanan/del/{id}',[DetailOrderLayanan_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Detail Purchase Order
|--------------------------------------------------------------------------
*/
Route::get('/dporder',[DetailPOrder_Ctrl::class,'index']);
Route::post('/dporder/save',[DetailPOrder_Ctrl::class,'simpan']);
Route::get('/listdporder',[DetailPOrder_Ctrl::class,'list']);
Route::get('/dporder/edit/{id}',[DetailPOrder_Ctrl::class,'edit']);
Route::get('/dporder/del/{id}',[DetailPOrder_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Instalasi
|--------------------------------------------------------------------------
*/
Route::get('/installasi',[Installasi_Ctrl::class,'index']);
Route::post('/installasi/save',[Installasi_Ctrl::class,'simpan']);
Route::get('/listinstallasi',[Installasi_Ctrl::class,'list']);
Route::get('/installasi/edit/{id}',[Installasi_Ctrl::class,'edit']);
Route::get('/installasi/del/{id}',[Installasi_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Inventory
|--------------------------------------------------------------------------
*/
Route::get('/inventory',[Inventory_Ctrl::class,'index']);
Route::post('/inventory/save',[Inventory_Ctrl::class,'simpan']);
Route::get('/listinventory',[Inventory_Ctrl::class,'list']);
Route::get('/inventory/edit/{id}',[Inventory_Ctrl::class,'edit']);
Route::get('/inventory/del/{id}',[Inventory_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Layanan
|--------------------------------------------------------------------------
*/
Route::get('/layanan',[Layanan_Ctrl::class,'index']);
Route::post('/layanan/save',[Layanan_Ctrl::class,'simpan']);
Route::get('/listlayanan',[Layanan_Ctrl::class,'list']);
Route::get('/layanan/edit/{id}',[Layanan_Ctrl::class,'edit']);
Route::get('/layanan/del/{id}',[Layanan_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Maintenance
|--------------------------------------------------------------------------
*/
Route::get('/maintenance',[Maintenance_Ctrl::class,'index']);
Route::post('/maintenance/save',[Maintenance_Ctrl::class,'simpan']);
Route::get('/listmaintenance',[Maintenance_Ctrl::class,'list']);
Route::get('/maintenance/edit/{id}',[Maintenance_Ctrl::class,'edit']);
Route::get('/maintenance/del/{id}',[Maintenance_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Order
|--------------------------------------------------------------------------
*/
Route::get('/order',[Order_Ctrl::class,'index']);
Route::post('/order/save',[Order_Ctrl::class,'simpan']);
Route::get('/listorder',[Order_Ctrl::class,'list']);
Route::get('/order/edit/{id}',[Order_Ctrl::class,'edit']);
Route::get('/order/del/{id}',[Order_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Paket
|--------------------------------------------------------------------------
*/
Route::get('/paket',[Paket_Ctrl::class,'index']);
Route::post('/paket/save',[Paket_Ctrl::class,'simpan']);
Route::get('/listpaket',[Paket_Ctrl::class,'list']);
Route::get('/paket/edit/{id}',[Paket_Ctrl::class,'edit']);
Route::get('/paket/del/{id}',[Paket_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Project
|--------------------------------------------------------------------------
*/
Route::get('/project',[Proyek_Ctrl::class,'index']);
Route::post('/project/save',[Proyek_Ctrl::class,'simpan']);
Route::get('/listproject',[Proyek_Ctrl::class,'list']);
Route::get('/project/edit/{id}',[Proyek_Ctrl::class,'edit']);
Route::get('/project/del/{id}',[Proyek_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Purchase Order
|--------------------------------------------------------------------------
*/
Route::get('/porder',[PurchaseOrder_Ctrl::class,'index']);
Route::post('/porder/save',[PurchaseOrder_Ctrl::class,'simpan']);
Route::get('/listporder',[PurchaseOrder_Ctrl::class,'list']);
Route::get('/porder/edit/{id}',[PurchaseOrder_Ctrl::class,'edit']);
Route::get('/porder/del/{id}',[PurchaseOrder_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Request
|--------------------------------------------------------------------------
*/
Route::get('/req',[Request_Ctrl::class,'index']);
Route::post('/req/save',[Request_Ctrl::class,'simpan']);
Route::get('/listreq',[Request_Ctrl::class,'list']);
Route::get('/req/edit/{id}',[Request_Ctrl::class,'edit']);
Route::get('/req/del/{id}',[Request_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Survey
|--------------------------------------------------------------------------
*/
Route::get('/survey',[Survey_Ctrl::class,'index']);
Route::post('/survey/save',[Survey_Ctrl::class,'simpan']);
Route::get('/listsurvey',[Survey_Ctrl::class,'list']);
Route::get('/survey/edit/{id}',[Survey_Ctrl::class,'edit']);
Route::get('/survey/del/{id}',[Survey_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Task
|--------------------------------------------------------------------------
*/
Route::get('/task',[Task_Ctrl::class,'index']);
Route::post('/task/save',[Task_Ctrl::class,'simpan']);
Route::get('/listtask',[Task_Ctrl::class,'list']);
Route::get('/task/edit/{id}',[Task_Ctrl::class,'edit']);
Route::get('/task/del/{id}',[Task_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| User
|--------------------------------------------------------------------------
*/
Route::get('/user',[User_Ctrl::class,'index']);
Route::post('/user/save',[User_Ctrl::class,'simpan']);
Route::get('/listuser',[User_Ctrl::class,'list']);
Route::get('/user/edit/{id}',[User_Ctrl::class,'edit']);
Route::get('/user/del/{id}',[User_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Vendor
|--------------------------------------------------------------------------
*/
Route::get('/vendor',[Vendor_Ctrl::class,'index']);
Route::post('/vendor/save',[Vendor_Ctrl::class,'simpan']);
Route::get('/listvendor',[Vendor_Ctrl::class,'list']);
Route::get('/vendor/edit/{id}',[Vendor_Ctrl::class,'edit']);
Route::get('/vendor/del/{id}',[Vendor_Ctrl::class,'delete']);

/*
|--------------------------------------------------------------------------
| Downgrade/Upgrade
|--------------------------------------------------------------------------
*/
Route::get('downup',[DownUp_Ctrl::class,'index']);
Route::post('downup/save',[DownUp_Ctrl::class,'simpan']);
Route::get('/listdownup',[DownUp_Ctrl::class,'list']);
Route::get('downup/edit/{id}',[DownUp_Ctrl::class,'edit']);
Route::get('downup/del/{id}',[DownUp_Ctrl::class,'delete']);
