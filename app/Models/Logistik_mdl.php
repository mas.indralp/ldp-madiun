<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Logistik_mdl extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = "ldp_logistik";
}
