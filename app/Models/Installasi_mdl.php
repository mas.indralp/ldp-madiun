<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Installasi_mdl extends Model
{
    use HasFactory;
    //protected $fillable = ['kode','nama','jumlah'];
    //protected $guarded = ['id'];
    protected $table = "ldp_instalasi";
    protected $primaryKey = 'id_instalasi';

}
