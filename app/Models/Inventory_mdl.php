<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory_mdl extends Model
{
    use HasFactory;
    use HasFactory;
    //protected $fillable = ['kode','nama','jumlah'];
    //protected $guarded = ['id'];
    protected $table = "ldp_inventory";
    protected $primaryKey = 'id_inventory';
}
