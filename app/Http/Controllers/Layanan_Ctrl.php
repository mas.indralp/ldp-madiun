<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Layanan_mdl;
use DB;

class Layanan_Ctrl extends Controller
{
    public function index()
    {
        return view ('pages.input.layanan',[
            "posts" => Layanan_mdl::all()
        ]
        );
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        Layanan_mdl::updateOrCreate(
            [
                "id_layanan" => $request->input("id")
            ],
            [
                "nama_layanan" => $request->input("nama"),
                "desk_layanan" => $request->input("diskripsi"),
                "harga_layanan" => $request->input("harga"),
                                
            ]              
        );
        return redirect('layanan'); 
    }

    public function list()
    {
        //$qdata = DB::table('ldp_instalasi')->get();
        $qdata = Layanan_mdl::all();
        return view('pages.list.layanan',['data'=>$qdata]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $qdata = Layanan_mdl::findOrFail($id);     
        return view('pages.input.layanan',compact('qdata'));        
    }

    public function delete($id) {

        $qdata = Layanan_mdl::find($id);    
        $qdata->delete();    
        return redirect('/listlayanan');
    
    }
}
