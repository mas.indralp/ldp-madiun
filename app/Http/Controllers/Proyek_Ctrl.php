<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Proyek_mdl;
use App\Models\Customer_mdl;
use DB;

class Proyek_Ctrl extends Controller
{
    public function index()
    {
        return View('pages.input.project')
        ->with('posts', Proyek_mdl::all())
        ->with('cust', Customer_mdl::all());
               
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        Proyek_mdl::updateOrCreate(
            [
                "id_project" => $request->input("id")
            ],
            [
                "tgl_project" => $request->input("tgl"),
                "nm_project" => $request->input("nama"),
                "desk_project" => $request->input("diskripsi"),               
                "id_pelanggan" => $request->input("idpel"),               
                "owner_project" => $request->input("owner"),               
                "status" => $request->input("status"),               
                
            ]              
        );
        return redirect('project'); 
    }

    public function list()
    {
        $qdata = Proyek_mdl::all();
        return view('pages.list.project',['data'=>$qdata]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $qdata = Proyek_mdl::findOrFail($id);     
        return view('pages.input.project',compact('qdata'));        
    }

    public function delete($id) {

        $qdata = Proyek_mdl::find($id);    
        $qdata->delete();    
        return redirect('/listproject');
    
    }
}
