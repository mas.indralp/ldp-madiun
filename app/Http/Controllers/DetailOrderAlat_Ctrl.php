<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DetailOrderAlat_mdl;
use App\Models\Inventory_mdl;
use App\Models\karyawan_mdl;
use DB;
class DetailOrderAlat_Ctrl extends Controller
{
     //
     public function index()
     {
        // return view ('pages.input.doalat',[
        //     "posts" => DetailOrderAlat_mdl::all()
         //]
        // );

         return View('pages.input.doalat')
            ->with('posts', DetailOrderAlat_mdl::all())
            ->with('inven', Inventory_mdl::all())
            ->with('karyawan', karyawan_mdl::all());
           
     }
 
     public function simpan(Request $request)
     {
         // $this->validate($request, [
         //     //This will be unique in users table
         //     'kode' => 'required|unique:kode',
         //     'nama' => 'required|min:5',
         //     'jumlah' => 'required|min:6',
         // ]);
        
         
         DetailOrderAlat_mdl::updateOrCreate(
             [
                 "id_order" => $request->input("id")
             ],
             [
                 "id_inventory" => $request->input("idinven"),
                 "tgl_ambil" => $request->input("tgl"),
                 "id_karyawan" => $request->input("idkary"),
             ]              
         );
         return redirect('doalat'); 
     }
 
     public function list()
     {
        $qdata = DetailOrderAlat_mdl::all();
         return view('pages.list.doalat',['data'=>$qdata]);
     }
    
     public function edit($id)
     {
         // mengambil data barang berdasarkan id yang dipilih
         $qdata = DetailOrderAlat_mdl::findOrFail($id);     
         return view('pages.input.doalat',compact('qdata'));        
     }
 
     public function delete($id) {
 
         $qdata = DetailOrderAlat_mdl::find($id);    
         $qdata->delete();    
         return redirect('/listdoalat');
     
     }
}
