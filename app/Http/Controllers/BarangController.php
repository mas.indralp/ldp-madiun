<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\inputbarang;
use DB;

class BarangController extends Controller
{
    //
    public function index()
    {
        return view ('pages.input.barang',[
            "posts" => inputbarang::all()
        ]
        );
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);

        inputbarang::updateOrCreate(
            [
                "id" => $request->input("id")
            ],
            [
                "kode" => $request->input("kodebrg"),
                "nama" => $request->input("namabrg"),
                "jumlah" => $request->input("jumlahbrg"),
            ]
        );
        return redirect('barang'); 
    }

    public function list()
    {
        $brg = DB::table('barangs')->get();
        return view('pages.list.barang',['dtbarang'=>$brg]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $brg = inputbarang::findOrFail($id);
        // passing data barang yang didapat ke view barang.blade.php
        //return view('pages.input.barang',['editbarang' => $brg]);        
        return view('pages.input.barang',compact('brg'));        
    }

    public function delete($id) {

        $brg = inputbarang::find($id);    
        $brg->delete();    
        return redirect('/listbarang');
    
    }

}
