<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Survey_mdl;
use App\Models\Customer_mdl;
use App\Models\Layanan_mdl;
use DB;


class Survey_Ctrl extends Controller
{
    public function index()
    {
        return View('pages.input.survey')
        ->with('posts', Survey_mdl::all())
        ->with('layanan', Layanan_mdl::all())
        ->with('cust', Customer_mdl::all());
           
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        Survey_mdl::updateOrCreate(
            [
                "id_survey" => $request->input("id")
            ],
            [
                "tgl_mulai" => $request->input("tgl1"),
                "tgl_selesai" => $request->input("tgl2"),
                "id_pelanggan" => $request->input("pelanggan"),               
                "geo" => $request->input("geo"),               
                "layanan" => $request->input("layanan"),               
                "paket" => $request->input("paket"),               
                "keterangan" => $request->input("ket"),               
                "rfs" => $request->input("rfs"),               
                "status" => $request->input("status"),               
                
            ]              
        );
        return redirect('survey'); 
    }

    public function list()
    {
        $qdata = Survey_mdl::all();
        return view('pages.list.survey',['data'=>$qdata]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $qdata = Survey_mdl::findOrFail($id);     
        return view('pages.input.survey',compact('qdata'));        
    }

    public function delete($id) {

        $qdata = Survey_mdl::find($id);    
        $qdata->delete();    
        return redirect('/listsurvey');
    
    }
}
