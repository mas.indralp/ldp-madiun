<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order_mdl;
use App\Models\Customer_mdl;
use DB;

class Order_Ctrl extends Controller
{
    public function index()
    {
        return View('pages.input.order')
	->with('posts', Order_mdl::all())
	->with('cust', Customer_mdl::all());   
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        Order_mdl::updateOrCreate(
            [
                "id_order" => $request->input("id")
            ],
            [
                "no_order" => $request->input("noorder"),
                "tgl_order" => $request->input("tgl_order"),
                "id_pelanggan" => $request->input("idpel"),
                "media" => $request->input("media"),
                "note" => $request->input("note"),
                "tgl_installasi" => $request->input("tglins"),
                "tgl_aktivasi" => $request->input("tglakt"),
                "total" => $request->input("total"),
                "status" => $request->input("solusi"),
                                
            ]              
        );
        return redirect('order'); 
    }

    public function list()
    {
        $qdata = Order_mdl::all();
        return view('pages.list.order',['data'=>$qdata]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $qdata = Order_mdl::findOrFail($id);     
        return view('pages.input.order',compact('qdata'));        
    }

    public function delete($id) {

        $qdata = Order_mdl::find($id);    
        $qdata->delete();    
        return redirect('/listorder');
    
    }
}
