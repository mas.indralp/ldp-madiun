<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task_mdl;
use App\Models\Karyawan_mdl;
use App\Models\Proyek_mdl;
use DB;
class Task_Ctrl extends Controller
{
    public function index()
    {
        return View('pages.input.task')
        ->with('posts', Task_mdl::all())
        ->with('project', Proyek_mdl::all())
        ->with('kary', Karyawan_mdl::all());
        
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        Task_mdl::updateOrCreate(
            [
                "id_task" => $request->input("id")
            ],
            [
                "id_project" => $request->input("project"),
                "tgl_start" => $request->input("tgl1"),
                "tgl_end" => $request->input("tgl2"),               
                "nm_task" => $request->input("nama"),               
                "desk_task" => $request->input("diskripsi"),               
                "id_karyawan" => $request->input("kary"),               
                "prioritas" => $request->input("prioritas"),               
                "jenis" => $request->input("jenis"),               
                "status" => $request->input("status"),               
                
            ]              
        );
        return redirect('task'); 
    }

    public function list()
    {
        $qdata = Task_mdl::all();
        return view('pages.list.task',['data'=>$qdata]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $qdata = Task_mdl::findOrFail($id);     
        return view('pages.input.task',compact('qdata'));        
    }

    public function delete($id) {

        $qdata = Task_mdl::find($id);    
        $qdata->delete();    
        return redirect('/listtask');
    
    }
}
