<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vendor_mdl;
use DB;

class Vendor_Ctrl extends Controller
{
    public function index()
    {
        return view ('pages.input.vendor',[
            "posts" => Vendor_mdl::all()
        ]
        );
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        Vendor_mdl::updateOrCreate(
            [
                "id_vendor" => $request->input("id")
            ],
            [
                "nama_vendor" => $request->input("nama"),
                "alamat" => $request->input("alamat"),
                "telp" => $request->input("telp"),               
                "owner" => $request->input("owner"),               
                "diskripsi" => $request->input("diskripsi"),               
                                
            ]              
        );
        return redirect('vendor'); 
    }

    public function list()
    {
        $qdata = Vendor_mdl::all();
        return view('pages.list.vendor',['data'=>$qdata]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $qdata = Vendor_mdl::findOrFail($id);     
        return view('pages.input.vendor',compact('qdata'));        
    }

    public function delete($id) {

        $qdata = Vendor_mdl::find($id);    
        $qdata->delete();    
        return redirect('/listvendor');
    
    }
}
