<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Request_mdl;
use App\Models\Inventory_mdl;
use App\Models\Karyawan_mdl;
use DB;

class Request_Ctrl extends Controller
{
    public function index()
    {
        return View('pages.input.req')
        ->with('posts', Request_mdl::all())
        ->with('inven', Inventory_mdl::all())
        ->with('karyawan', Karyawan_mdl::all());
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        Request_mdl::updateOrCreate(
            [
                "id_req_order" => $request->input("id")
            ],
            [
                "tgl_req" => $request->input("tgl"),
                "id_inventory" => $request->input("inventory"),
                "nama_barang" => $request->input("nama"),               
                "harga" => $request->input("harga"),               
                "jumlah" => $request->input("jumlah"),               
                "id_karyawan" => $request->input("karyawan"),               
                "status" => $request->input("status"),               
                
            ]              
        );
        return redirect('req'); 
    }

    public function list()
    {
        $qdata = Request_mdl::all();
        return view('pages.list.req',['data'=>$qdata]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $qdata = Request_mdl::findOrFail($id);     
        return view('pages.input.req',compact('qdata'));        
    }

    public function delete($id) {

        $qdata = Request_mdl::find($id);    
        $qdata->delete();    
        return redirect('/listreq');
    
    }
}
