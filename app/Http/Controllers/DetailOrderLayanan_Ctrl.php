<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DetailOrderLayanan_mdl;
use App\Models\Layanan_mdl;
use DB;

class DetailOrderLayanan_Ctrl extends Controller
{
    public function index()
    {
        return View('pages.input.dolayanan')
        ->with('posts', DetailOrderLayanan_mdl::all())
        ->with('layanan', Layanan_mdl::all());     
       
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        DetailOrderLayanan_mdl::updateOrCreate(
            [
                "id_order" => $request->input("id")
            ],
            [
                "id_layanan" => $request->input("idlayanan"),
                "harga_layanan" => $request->input("harga"),
                
            ]              
        );
        return redirect('dolayanan'); 
    }

    public function list()
    {
        $qdata = DetailOrderLayanan_mdl::all();
        return view('pages.list.dolayanan',['data'=>$qdata]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $qdata = DetailOrderLayanan_mdl::findOrFail($id);     
        return view('pages.input.dolayanan',compact('qdata'));        
    }

    public function delete($id) {

        $qdata = DetailOrderLayanan_mdl::find($id);    
        $qdata->delete();    
        return redirect('/listdolayanan');
    
    }
}
