<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\modelkaryawan;
use DB;

class KaryawanController extends Controller
{
    //
    public function index()
    {
        return view ('pages.input.karyawan',[
            "posts" => modelkaryawan::all()
        ]
        );
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        modelkaryawan::updateOrCreate(
            [
                "id" => $request->input("id")
            ],
            [
                "nip" => $request->input("nopel"),
                "nama" => $request->input("namapel"),
                "alamat" => $request->input("nik"),
                "telp" => $request->input("jabatan"),
                "npwp" => $request->input("perusahaan"),
                "jabatan" => $request->input("alamat"),
                "tglmasuk" => $request->input("kodepos"),
                "status" => $request->input("kodepos"),
            ]
        );
        return redirect('karyawan'); 
    }

    public function list()
    {
        $kary = DB::table('ldp_karyawan')->get();
        return view('pages.list.karyawan',['dtkary'=>$kary]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $kary = modelkaryawan::findOrFail($id);
        // passing data barang yang didapat ke view barang.blade.php
        //return view('pages.input.barang',['editbarang' => $brg]);        
        return view('pages.input.karyawan',compact('kary'));        
    }

    public function delete($id) {

        $kary = modelkaryawan::find($id);    
        $kary->delete();    
        return redirect('/listkaryawan');
    
    }
}
