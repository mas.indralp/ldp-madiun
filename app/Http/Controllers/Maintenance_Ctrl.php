<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Maintenance_mdl;
use App\Models\Customer_mdl;
use DB;
class Maintenance_Ctrl extends Controller
{
    public function index()
    {
        return View('pages.input.maintenance')
	->with('posts', Maintenance_mdl::all())
	->with('cust', Customer_mdl::all());
   
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        Maintenance_mdl::updateOrCreate(
            [
                "id_maintenance" => $request->input("id")
            ],
            [
                "id_pelanggan" => $request->input("idpel"),
                "tgl_mulai" => $request->input("tgl1"),
                "tgl_selesai" => $request->input("tgl2"),
                "keluhan" => $request->input("keluhan"),
                "solusi" => $request->input("solusi"),
                                
            ]              
        );
        return redirect('maintenance'); 
    }

    public function list()
    {
        $qdata = Maintenance_mdl::all();
        return view('pages.list.maintenance',['data'=>$qdata]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $qdata = Maintenance_mdl::findOrFail($id);     
        return view('pages.input.maintenance',compact('qdata'));        
    }

    public function delete($id) {

        $qdata = Maintenance_mdl::find($id);    
        $qdata->delete();    
        return redirect('/listmaintenance');
    
    }
}
