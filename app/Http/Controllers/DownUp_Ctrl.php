<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DownUp_mdl;
use App\Models\Karyawan_mdl;
use App\Models\Customer_mdl;
use DB;

class DownUp_Ctrl extends Controller
{
    public function index()
    {
        return View('pages.input.downup')
        ->with('posts', DownUp_mdl::all())
        ->with('kary', Karyawan_mdl::all())
        ->with('cust', Customer_mdl::all());
     
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        Logistik_mdl::updateOrCreate(
            [
                "id" => $request->input("id")
            ],
            [
                "tgl" => $request->input("tgl"),
                "perangkat" => $request->input("perangkat"),
                "tipe" => $request->input("tipe"),
                "jumlah" => $request->input("jumlah"),
                "alokasi" => $request->input("lokasi"),
                "pelanggan" => $request->input("pelanggan"), 
                "nip" => $request->input("kary"), 
            ]
        );
        return redirect('logistik'); 
    }

    public function list()
    {
        $logistik = DB::table('ldp_logistik')->get();
        return view('pages.list.logistik',['dtkary'=>$logistik]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $logistik = Logistik_mdl::findOrFail($id);
        // passing data barang yang didapat ke view barang.blade.php
        //return view('pages.input.barang',['editbarang' => $brg]);        
        return view('pages.input.logistik',compact('logistik'));        
    }

    public function delete($id) {

        $logistik = Logistik_mdl::find($id);    
        $logistik->delete();    
        return redirect('/listlogistik');
    
    }
}
