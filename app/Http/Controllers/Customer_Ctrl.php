<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\modelcustomer;
use DB;

class CustomerController extends Controller
{
    //
    public function index()
    {
        return view ('pages.input.cust',[
            "posts" => modelcustomer::all()
        ]
        );
    }

    public function index_1()
    {
        return view ('pages.input.cust_1',[
            "posts" => modelcustomer::all()
        ]
        );
    }


    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        modelcustomer::updateOrCreate(
            [
                "id" => $request->input("id")
            ],
            [
                "no_pelanggan" => $request->input("nopel"),
                "pj" => $request->input("namapel"),
                "nik" => $request->input("nik"),
                "jabatan" => $request->input("jabatan"),
                "perusahaan" => $request->input("perusahaan"),
                "alamat" => $request->input("alamat"),
                "kodepos" => $request->input("kodepos"),
                "telp" => $request->input("telp"),
                "email" => $request->input("email"),
                "coordinat" => $request->input("koordinat"),
                "tech_ctc" => $request->input("tech_ct"),
                "tech_telp" => $request->input("tech_telp"),
                "tech_email" => $request->input("tech_email"),
                "fin_ctc" => $request->input("fin_ct"),
                "fin_telp" => $request->input("fin_telp"),
                "fin_email" => $request->input("fin_email"),
                "layanan" => $request->input("layanan"),
                "port_bndwt" => $request->input("port_bnwt"),
                "media" => $request->input("media"),
                "media_oleh" => $request->input("mediaoleh"),
                "npwp" => $request->input("nonpwp"),
                "alamat_npwp" => $request->input("alamatnpwp"),
                "jns_setoran" => $request->input("setoran"),
                "biaya_bulanan" => $request->input("biaya"),
            ]
        );
        return redirect('customer'); 
    }

    public function list()
    {
        $cust = DB::table('ldp_cust')->get();
        return view('pages.list.cust',['dtcust'=>$cust]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $cust = modelcustomer::findOrFail($id);
        // passing data barang yang didapat ke view barang.blade.php
        //return view('pages.input.barang',['editbarang' => $brg]);        
        return view('pages.input.cust',compact('cust'));        
    }

    public function delete($id) {

        $cust = modelcustomer::find($id);    
        $cust->delete();    
        return redirect('/listcustomer');
    
    }



    
    public function simpan_1(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        modelcustomer::updateOrCreate(
            [
                "id" => $request->input("id")
            ],
            [
                "no_pelanggan" => $request->input("nopel"),
                "pj" => $request->input("namapel"),
                "nik" => $request->input("nik"),
                "alamat" => $request->input("alamat"),
                "telp" => $request->input("telp"),
                "coordinat" => $request->input("koordinat"),
                "layanan" => $request->input("layanan"),
                "port_bndwt" => $request->input("port_bnwt"),
                "media" => $request->input("media"),
                "media_oleh" => $request->input("mediaoleh"),
                "progress" => $request->input("progress"),
               
            ]
        );
        return redirect('customer_1'); 
    }
    
    public function list_1()
    {
        $cust = DB::table('ldp_cust')->get();
        return view('pages.list.cust_1',['dtcust'=>$cust]);
    }
   
    public function edit_1($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $cust = modelcustomer::findOrFail($id);
        // passing data barang yang didapat ke view barang.blade.php
        //return view('pages.input.barang',['editbarang' => $brg]);        
        return view('pages.input.cust_1',compact('cust'));        
    }

    public function delete_1($id) {

        $cust = modelcustomer::find($id);    
        $cust->delete();    
        return redirect('/listcustomer_1');
    
    }
}
