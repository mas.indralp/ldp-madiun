<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\installasi_mdl;
use App\Models\Order_mdl;
use DB;

class Installasi_Ctrl extends Controller
{
    public function index()
    {
        return View('pages.input.installasi')
        ->with('posts', installasi_mdl::all())
        ->with('order', Order_mdl::all());
  
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        installasi_mdl::updateOrCreate(
            [
                "id_instalasi" => $request->input("id")
            ],
            [
                "id_order" => $request->input("idorder"),
                "tgl_mulai" => $request->input("tglmulai"),
                "tgl_selesai" => $request->input("tglselesai"),
                "layanan" => $request->input("layanan"),
                "interface" => $request->input("interface"),
                "LO" => $request->input("lo"),
                "LT" => $request->input("lt"),
                "perangkat" => $request->input("perangkat"),
                "bukti" => $request->input("bukti"),
                "status" => $request->input("status"),
                
            ]              
        );
        return redirect('installasi'); 
    }

    public function list()
    {
        $qdata = installasi_mdl::all();
        return view('pages.list.installasi',['data'=>$qdata]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $qdata = installasi_mdl::findOrFail($id);     
        return view('pages.input.installasi',compact('qdata'));        
    }

    public function delete($id) {

        $qdata = installasi_mdl::find($id);    
        $qdata->delete();    
        return redirect('/listinstallasi');
    
    }
}
