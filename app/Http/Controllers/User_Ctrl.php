<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User_mdl;
use DB;

class User_Ctrl extends Controller
{
    public function index()
    {
        return view ('pages.input.user',[
            "posts" => User_mdl::all()
        ]
        );
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        User_mdl::updateOrCreate(
            [
                "user_id" => $request->input("id")
            ],
            [
                "user_name" => $request->input("name"),
                "user_email" => $request->input("email1"),
                "user_password" => $request->input("pass"),               
                "user_role" => $request->input("role"),               
                "id_reff" => $request->input("reff"),               
                "remember_token" => $request->input("token"),               
                "email_validate_at" => $request->input("email2"),                              
                "status" => $request->input("status"),               
                
            ]              
        );
        return redirect('user'); 
    }

    public function list()
    {
        $qdata = User_mdl::all();
        return view('pages.list.user',['data'=>$qdata]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $qdata = User_mdl::findOrFail($id);     
        return view('pages.input.user',compact('qdata'));        
    }

    public function delete($id) {

        $qdata = User_mdl::find($id);    
        $qdata->delete();    
        return redirect('/listuser');
    
    }
}
