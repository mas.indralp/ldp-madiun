<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DetailPOrder_mdl;
use App\Models\Inventory_mdl;
use DB;

class DetailPOrder_Ctrl extends Controller
{
    public function index()
    {
        return View('pages.input.dporder')
            ->with('posts', DetailPOrder_mdl::all())
            ->with('inven', Inventory_mdl::all());
               
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        DetailPOrder_mdl::updateOrCreate(
            [
                "id_po" => $request->input("idorder")
            ],
            [
                "id_inventory" => $request->input("idinven"),
                "harga" => $request->input("harga"),
                "jml_order" => $request->input("jmlorder"),
                "jml_datang" => $request->input("jmldatang"),
                "alokasi" => $request->input("alokai"),
                "desk_dpo" => $request->input("diskripsi"),
                
            ]              
        );
        return redirect('dporder'); 
    }

    public function list()
    {
        $qdata = DetailPOrder_mdl::all();
        return view('pages.list.dporder',['data'=>$qdata]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $qdata = DetailPOrder_mdl::findOrFail($id);     
        return view('pages.input.dporder',compact('qdata'));        
    }

    public function delete($id) {

        $qdata = DetailPOrder_mdl::find($id);    
        $qdata->delete();    
        return redirect('/listdporder');
    
    }
}
