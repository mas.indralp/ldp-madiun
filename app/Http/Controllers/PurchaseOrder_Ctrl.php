<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PurchaseOrder_mdl;
use App\Models\Vendor_mdl;
use DB;

class PurchaseOrder_Ctrl extends Controller
{
    public function index()
    {
        return View('pages.input.porder')
        ->with('posts', PurchaseOrder_mdl::all())
        ->with('vendor', Vendor_mdl::all());
           
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        PurchaseOrder_mdl::updateOrCreate(
            [
                "id_po" => $request->input("id")
            ],
            [
                "tgl_po" => $request->input("tgl"),
                "tgl_tempo_po" => $request->input("tgltempo"),
                "id_vendor" => $request->input("vendor"),               
                "tipe_bayar" => $request->input("bayar"),               
                "note" => $request->input("note"),               
                "status" => $request->input("status"),               
                
            ]              
        );
        return redirect('porder'); 
    }

    public function list()
    {
        $qdata = PurchaseOrder_mdl::all();
        return view('pages.list.porder',['data'=>$qdata]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $qdata = PurchaseOrder_mdl::findOrFail($id);     
        return view('pages.input.porder',compact('qdata'));        
    }

    public function delete($id) {

        $qdata = PurchaseOrder_mdl::find($id);    
        $qdata->delete();    
        return redirect('/listporder');
    
    }
}
