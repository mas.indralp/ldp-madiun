<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paket_mdl;
use App\Models\Layanan_mdl;
use DB;
class Paket_Ctrl extends Controller
{
    public function index()
    {
        return View('pages.input.paket')
	->with('posts', Paket_mdl::all())
	->with('inven', Layanan_mdl::all());
   
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        Paket_mdl::updateOrCreate(
            [
                "id_pelanggan" => $request->input("id")
            ],
            [
                "id_layanan" => $request->input("idlayanan"),
                "kecepatan" => $request->input("kecepatan"),
                "other" => $request->input("other"),               
            ]              
        );
        return redirect('paket'); 
    }

    public function list()
    {
        $qdata = Paket_mdl::all();
        return view('pages.list.paket',['data'=>$qdata]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $qdata = Paket_mdl::findOrFail($id);     
        return view('pages.input.paket',compact('qdata'));        
    }

    public function delete($id) {

        $qdata = Paket_mdl::find($id);    
        $qdata->delete();    
        return redirect('/listpaket');
    
    }
}
