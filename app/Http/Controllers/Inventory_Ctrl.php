<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inventory_mdl;
use App\Models\Vendor_mdl;
use DB;

class Inventory_Ctrl extends Controller
{
    public function index()
    {
        return View('pages.input.inventory')
        ->with('vendor', Vendor_mdl::all())
        ->with('inven', Inventory_mdl::all());
      
    }

    public function simpan(Request $request)
    {
        // $this->validate($request, [
        //     //This will be unique in users table
        //     'kode' => 'required|unique:kode',
        //     'nama' => 'required|min:5',
        //     'jumlah' => 'required|min:6',
        // ]);
       
        
        Inventory_mdl::updateOrCreate(
            [
                "id_inventory" => $request->input("id")
            ],
            [
                "sn_inventory" => $request->input("sn"),
                "nama" => $request->input("nama"),
                "id_vendor" => $request->input("idvendor"),
                "tgl_beli" => $request->input("tgl"),
                "harga_beli" => $request->input("harga"),
                "desk_inventory" => $request->input("diskripsi"),
                "kat_inventory" => $request->input("kategori"),
                "kondisi" => $request->input("kondisi"),
                "status" => $request->input("status"),
                
            ]              
        );
        return redirect('inventory'); 
    }

    public function list()
    {
        $qdata = Inventory_mdl::all();
        return view('pages.list.inventory',['data'=>$qdata]);
    }
   
    public function edit($id)
    {
        // mengambil data barang berdasarkan id yang dipilih
        $qdata = Inventory_mdl::findOrFail($id);     
        return view('pages.input.inventory',compact('qdata'));        
    }

    public function delete($id) {

        $qdata = Inventory_mdl::find($id);    
        $qdata->delete();    
        return redirect('/listinventory');
    
    }
}
