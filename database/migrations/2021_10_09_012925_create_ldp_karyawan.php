<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLdpKaryawan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ldp_karyawan', function (Blueprint $table) {
            $table->id();
            $table->text('nip');
            $table->text('nama');
            $table->text('alamat');
            $table->text('telp');
            $table->text('tempat_lhr');
            $table->text('tgl_lhr');
            $table->text('jk');
            $table->text('pendidikan');
            $table->text('npwp');
            $table->text('bpjs_tker');
            $table->text('jabatan');
            $table->text('tglmasuk');
            $table->text('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ldp_karyawan');
    }
}
