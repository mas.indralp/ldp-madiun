<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLdpLogistik extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ldp_logistik', function (Blueprint $table) {
            $table->id();
            $table->text('tanggal');
            $table->text('perangkat');
            $table->text('tipe');
            $table->text('jumlah');
            $table->text('alokasi');
            $table->text('pelanggan');
            $table->text('nip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ldp_logistik');
    }
}
