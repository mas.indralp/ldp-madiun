<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLdpCust extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ldp_cust', function (Blueprint $table) {
            $table->id();
            $table->text('no_pelanggan');
            $table->text('pj');
            $table->text('nik');
            $table->text('jabatan');
            $table->text('perusahaan');
            $table->text('alamat');
            $table->text('kodepos');
            $table->text('telp');
            $table->text('email');
            $table->text('coordinat');
            $table->text('tech_ctc');
            $table->text('tech_telp');
            $table->text('tech_email');
            $table->text('fin_ctc');
            $table->text('fin_telp');
            $table->text('fin_email');
            $table->text('layanan');
            $table->text('port_bndwt');
            $table->text('media');
            $table->text('media_oleh');
            $table->text('npwp');
            $table->text('alamat_npwp');
            $table->text('jns_setoran');
            $table->text('biaya_bulanan');      
            $table->timestamps();
        });
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ldp_cust');
    }
}
